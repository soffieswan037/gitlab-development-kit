---
stage: Ecosystem
group: Contributor Experience
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Start the Rails console

To start the Rails console in GDK:

```shell
cd <gdk-dir>/gitlab
bundle exec rails console
```

To exit the console, type: `quit` and press Enter.
